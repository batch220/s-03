# [Section] Lists
# Lists are similar to arrays in JavaScript in a sense that they can contain a collection of data.
# To create a list, the square brackets ([]) is used.
names = ["John", "Paul", "George", "Ringo"] #String list
durations = [260, 180, 20] # Number list
truth_values = [True, False, True, True, False] # Boolean list

# A list can contain elements of different data types:
sample_list = ["Apple", 3, False, "Potato", 4, True]
# Note: Problems may arise if you try to use lists with multiple types for something that expects them to be all the same type.

# Getting the list size
# The number of elements in a list can be counted using the len() method
print(len(durations))

# Accessing values
# Lists can be accessed by providing the index number of the element
# The index numbers in lists start at 0 and ends at (n-1), where n is the number of elements
# Access the first item in the list
print(names[0])  # John

# Accessing a range of values in a list
print(names[0:2])

# Updating Lists
# Print the current value
print(f'Current value: {names[2]}')

# Update the value
names[2] = 'Michael'

# Print the new value
print(f'New value: {names[2]}')

# List Manipulation
# Lists have methods that can be used to manipulate the elements within

# Adding List Items - the append() method allows inserting items to the end of the list

names.append('Bobby')
print(names)

# Deleting List Items - the "del" keyword can be used to delete items from a list
durations.append(360)
print(durations)

# Delete the last item from durations
del durations[-1]
print(durations)

# Membership checks - the "in" keyword checks if a given elements is in the list and returns true or false
print(20 in durations)
print(500 in durations)

# Sorting Lists - the sort() method sorts the list alphanumerically, in ascending order by default
names.sort()
print(names)

# Looping Through Lists
count = 0
while count < len(names):
    print(names[count])
    count += 1

# Dictionaries
# Dictionaries are used to store data in key:value pairs, similar to Objects in JavaScript. Dictionaries are collections which are ordered, changeable, and do not allow duplicates.

# Ordered means that items have a defined order that cannot be changed
# Changeable means that values can be changed
person1 = {
    "name": "Brandon",
    "age": 28,
    "occupation": "student",
    "isEnrolled": True,
    "subjects": ["Python", "SQL", "Django"]
}

# To get the number of key:value pairs in a dictionary, the len() method can be used again
print(len(person1))

# Accessing Values in Dictionaries
# To get the value of an item in a dictionary, the key name can be used with a pair of square brackets
print(person1["name"])

# The keys() and values() methods will return a list of all keys/values in the dictionary
print(person1.keys())

print(person1.values())

# Check the data type of a value
print(type(names))
print(type(person1))

# The items() method will return each item in the dictionary, as key:value pair in a list
print(person1.items())

# print(person1)

# Adding key:value pairs can be done by either putting a new index key and assigning a value or by using the update() method
person1["nationality"] = "Filipino"
person1.update({"fav_food": "BBQ"})
print(person1)

# Deleting entries can be done using the pop() method or the del keyword
person1.pop("fav_food")
del person1["nationality"]
print(person1)

# Looping through dictionaries
for key in person1:
    print(f"The value of {key} is {person1[key]}")

# Nested Dictionaries
person2 = {
    "name": "Monika",
    "age": 28,
    "occupation": "student",
    "isEnrolled": True,
    "subjects": ["Python", "SQL", "Django"]
}

classRoom = {
    "student1": person1,
    "student2": person2
}

print(classRoom)
))
# Functions
# Functions are blocks of code that can be run when called/invoked. A function can be used to get input, procees the input, and return output.

# The "def" keyword is used to create a function
def my_greeting():
    # code to be executed when function is invoked
    print('Hello user!')

# Calling/invoking the function
my_greeting()

# Parameters can be added to functions to have more control as to what input the function will need
def greet_user(username):
    # prints out the value of the username parameters
    print(f'Hello, {username}!')

# Arguments are the values represted by parameters
greet_user("Bob")
greet_user("Amy")

# Return statements - the "return" keyword allows functions to return values, just like in JavaScript

def addition(num1, num2):
    return num1 + num2

sum = addition(5, 10)
print(f"The sum is {sum}")

# Lambda Functions
# A Lambda Function is a small anonymous function that can be used for callbacks. It is just like any normal Python function, except that its name is defined as a variable, and usually contains just one line of code. A Lambda Function can take any number of arguments, but can only have one expression.

greeting = lambda person: f'Hello {person}'
print(greeting("Anthony"))

mult = lambda a, b : a * b
print(mult(5, 6))

def increaser(num1):
    return lambda num2: num2 * num1

doubler = increaser(2)

print(doubler(11))

tripler = increaser(3)

print(tripler(7
